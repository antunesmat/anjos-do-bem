class PagesController < ApplicationController
  skip_before_action :authenticate_admin! # Controlador de Páginas tem livre acesso.

  def home
  end
end
