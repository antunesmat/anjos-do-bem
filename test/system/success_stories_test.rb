require "application_system_test_case"

class SuccessStoriesTest < ApplicationSystemTestCase
  setup do
    @success_story = success_stories(:one)
  end

  test "visiting the index" do
    visit success_stories_url
    assert_selector "h1", text: "Success Stories"
  end

  test "creating a Success story" do
    visit success_stories_url
    click_on "New Success Story"

    fill_in "Description", with: @success_story.description
    fill_in "Owner", with: @success_story.owner
    fill_in "Pet name", with: @success_story.pet_name
    fill_in "Photo", with: @success_story.photo
    click_on "Create Success story"

    assert_text "Success story was successfully created"
    click_on "Back"
  end

  test "updating a Success story" do
    visit success_stories_url
    click_on "Edit", match: :first

    fill_in "Description", with: @success_story.description
    fill_in "Owner", with: @success_story.owner
    fill_in "Pet name", with: @success_story.pet_name
    fill_in "Photo", with: @success_story.photo
    click_on "Update Success story"

    assert_text "Success story was successfully updated"
    click_on "Back"
  end

  test "destroying a Success story" do
    visit success_stories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Success story was successfully destroyed"
  end
end
